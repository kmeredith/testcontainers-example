scalaVersion := "2.13.0"

lazy val doobieV	      = "0.13.2"
lazy val testContainersScalaV = "0.39.5"

libraryDependencies ++= Seq(
  "org.tpolecat" %% "doobie-core"              % doobieV,
  "org.tpolecat" %% "doobie-postgres"          % doobieV,
  "org.tpolecat" %% "doobie-scalatest"         % doobieV,
  "com.dimafeng" %% "testcontainers-scala" % testContainersScalaV % Test,
  "com.dimafeng" %% "testcontainers-scala-postgresql" % testContainersScalaV % Test
)

