# Purpose

The purpose of this repo is to demonstrate how to use [testcontainers-scala](https://github.com/testcontainers/testcontainers-scala) and [doobie](https://tpolecat.github.io/doobie/).

`doobie` is a pure Functional Programming Library in Scala for JDBC. What I personally like about `doobie` is that it's a thin layer over SQL. In other words, in my app, I write SQL directly, i.e. it's not an [Object-relational Mapping](https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping) tool.

In this demo, I'm using testcontainers-scala in order to stand up a Postgresql database within a docker container for a test. Once the DB is up and running, I then verify, at run-time, that my doobie SQL is valid via [Typechecking Queries](https://tpolecat.github.io/doobie/docs/06-Checking.html).

# Value

From my experience with cats-effect and doobie in prod, I always wrote this type of spec when building any code involving SQL. I did it since it was a cheap way to verify that my queries were actually correct, as well as mapped to the actual types in the database tables' schemas.

# How to Run

```scala
$sbt
sbt:doobie-test-containers> test
...
[info] MessagesSQLTypeQuerySpec:
[info] Messages#get
[info] - should typecheck *** FAILED ***
[info]   Query0[Int] defined at Messages.scala:9
[info]     select count(*) from messages
[info]     ✓ SQL Compiles and TypeChecks
[info]     ✕ C01 count BIGINT (int8) NULL?  →  Int
[info]       BIGINT (int8) is ostensibly coercible to Int according to the JDBC
[info]       specification but is not a recommended target type. Expected
[info]       schema type was INTEGER. (Checker.scala:56)
[info] Run completed in 7 seconds, 823 milliseconds.
[info] Total number of tests run: 1
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 0, failed 1, canceled 0, ignored 0, pending 0
[info] *** 1 TEST FAILED ***
[error] Failed tests:
[error] 	com.wh.MessagesSQLTypeQuerySpec
[error] (Test / test) sbt.TestsFailedException: Tests unsuccessful
[error] Total time: 9 s, completed May 26, 2021, 1:42:50 PM
```

# What about H2?

[H2](https://www.h2database.com/html/main.html) provides a means of testing SQL against an in-memory DB. In the past, before I knew about
doobie's type-checked query tests, I had used it. I prefer using doobie specs over H2 for two reasons:

* when I last used it ~1 year ago, H2's supported SQL was a subset of PostgreSQL/SQL standard. As I recall, due to H2's limitations, I had opted to change a datatype in my [Data Definition Language](https://en.wikipedia.org/wiki/Data_definition_language) from `UUID` to `varchar(128)` since H2 did not support `UUID`. If my testing tool results in using less precise types, then I'll look for alternatives, e.g. doobie's typechecked queries.
* running a spec against an actual running DB in a container is more realistic