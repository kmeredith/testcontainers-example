package com.wh

import cats.effect.{ContextShift, IO}
import cats.implicits._
import com.dimafeng.testcontainers.{ForAllTestContainer, PostgreSQLContainer}
import doobie.util.transactor.Transactor
import doobie.implicits._
import org.scalatest.flatspec.AnyFlatSpec
import scala.concurrent.ExecutionContext

abstract class PostgresqlSpec extends AnyFlatSpec with ForAllTestContainer  {

  private implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

  override lazy val container: PostgreSQLContainer =
    PostgreSQLContainer(dockerImageNameOverride = "postgres:13.3")

  override def afterStart(): Unit = {
    Migrations.create.run.void.transact(transactor).unsafeRunSync()
  }

  lazy val transactor: Transactor[IO] =
    Transactor.fromDriverManager[IO](
      container.driverClassName, container.jdbcUrl, container.username, container.password
    )
}
