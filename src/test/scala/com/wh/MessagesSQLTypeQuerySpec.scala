package com.wh

import cats.effect.IO
import doobie.scalatest.IOChecker
import doobie.util.transactor.Transactor
import org.scalatest._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers
import com.dimafeng.testcontainers.{ForAllTestContainer, MySQLContainer}

class MessagesSQLTypeQuerySpec
  extends PostgresqlSpec
    with Matchers
    with IOChecker {

  "Messages#get" should "typecheck" in  { check(Messages.get)  }

}