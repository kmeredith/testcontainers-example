package com.wh

import doobie.implicits._
import doobie.util.update.Update0

object Migrations {

  // In a prod app, I'd use Flyway or Liquibase for handling migrations
  // This is just a toy app, so I'm using an Update0. I'd never take this approach
  // in a real-world, prod app.
  def create: Update0 =
    sql"""
         create table if not exists messages ( author text not null, content text not null )
       """.update
}
