package com.wh

import doobie._
import doobie.implicits._
import doobie.util.query.Query0

object Messages {
  def get: Query0[Int] =
    sql"""select count(*) from messages""".query[Int]
}
